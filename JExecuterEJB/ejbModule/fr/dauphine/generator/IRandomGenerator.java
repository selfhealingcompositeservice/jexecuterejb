package fr.dauphine.generator;

import edu.uci.ics.jung.graph.Graph;
import fr.dauphine.service.Service;

public interface IRandomGenerator {

	Graph<Service, Number> generateRandomGraph(int vertexCount);

}
