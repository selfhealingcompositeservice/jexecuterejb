package fr.dauphine.generator;

import javax.ejb.Remote;
import javax.ejb.Stateless;

import edu.uci.ics.jung.graph.Graph;
import fr.dauphine.generator.IRandomGenerator;
import fr.dauphine.generator.transactional.RandomTransactionalProperty;
import fr.dauphine.graphutils.GraphUtils;
import fr.dauphine.random.RandomQoSGenerator;
import fr.dauphine.service.Service;

@Stateless
@Remote(IRandomGenerator.class)
public class RandomGeneratorBean implements IRandomGenerator {
	
	@Override
	public Graph<Service, Number> generateRandomGraph(int vertexCount) {

		GraphGenerator<Service, Number> graphGenerator = new GraphGenerator<>();
		Graph<Service, Number> graph = 
				GraphUtils.toServiceGraph(graphGenerator.generateRandomBarabasiAlbert(vertexCount));
		RandomTransactionalProperty.generateRandTP(graph);
		RandomQoSGenerator<Number> rQoS = new RandomQoSGenerator<>();
		rQoS.generateQoS(graph); 
		
		GraphUtils.generateConcepts(graph);
		
		return graph;
	}

}
