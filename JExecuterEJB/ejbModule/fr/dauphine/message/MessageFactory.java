package fr.dauphine.message;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.LinkedBlockingDeque;

import fr.dauphine.service.Constants;
import fr.dauphine.service.Data;
import fr.dauphine.service.Service;

public class MessageFactory {

	private Map<String, LinkedBlockingDeque<Message>> queues;
	
	
	public MessageFactory(Map<String, LinkedBlockingDeque<Message>> queues) {
		this.queues = queues;
	}
	
	public void send(List<Message> messages, Collection<Service> collection) {
		for(Message m:messages)
			for(Service s:collection)
				if(m instanceof DataMessage) {
					if(s.getInputs().contains(((DataMessage)m).getData()))
						queues.get(s.getName()).offer(m);
				} else 
					queues.get(s.getName()).offer(m);
	}
	
	/**
	 * Sends a message to the Execution Engine.
	 * Example of messages are compensate and checkpoint.
	 * @param message
	 */
	public void sendToExecutionEngine(Message message) {
					queues.get(Constants.FINAL_NODE).offer(message);
	}
	
	public static Message getCompensateMessage() {
		ControlMessage msg = new ControlMessage();
		msg.setMessage(ControlMessageType.COMPENSATE);
		return msg;
	}
	
	public static Message getCompensateTokenMessage() {
		ControlMessage msg = new ControlMessage();
		msg.setMessage(ControlMessageType.COMPENSATE_TOKEN);
		return msg;
	}
	
	public static Message getCheckpointMessage() {
		ControlMessage msg = new ControlMessage();
		msg.setMessage(ControlMessageType.SKIP);
		return msg;
	}

	public void send(Message message, Service s) {
		send(message, s.getName());
	}

	public void send(Message message, String s) {
		queues.get(s).offer(message);
		
	}

	public static Message getTerminateToken() {
		ControlMessage msg = new ControlMessage();
		msg.setMessage(ControlMessageType.TERMINATE);
		return msg;
	}

	public void send(Message message, Collection<Service> collection) {
		for(Service s:collection)
			queues.get(s.getName()).offer(message);
	}
	
}
