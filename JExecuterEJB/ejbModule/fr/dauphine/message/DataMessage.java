package fr.dauphine.message;

import java.io.Serializable;

import fr.dauphine.service.Data;

public class DataMessage implements Serializable, Message {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Data data;
	private String value;
	private double accumulatedTime;
	
	


	public DataMessage(Data data, String value) {
		this.setData(data);
		this.setValue(value);
	}


	public Data getData() {
		return data;
	}


	public void setData(Data data) {
		this.data = data;
	}


	public String getValue() {
		return value;
	}


	public void setValue(String value) {
		this.value = value;
	}


	@Override
	public String toString() {
		return "{name= " + data.getName() + ", value=" + value + "}";
	} 
	
	@Override
	public boolean equals(Object obj) {
		
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		
		if(!(obj instanceof DataMessage))
			return false;
		DataMessage d = (DataMessage) obj;
		
		return data.equals(d.getData()) && value.equals(d.getValue());
	}


	@Override
	public String getSender() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public void setSender(String sender) {
		// TODO Auto-generated method stub
		
	}


	public double getAccumulatedTime() {
		return accumulatedTime;
	}


	public void setAccumulatedTime(double accumulatedTime) {
		this.accumulatedTime = accumulatedTime;
	}
	
	
}
