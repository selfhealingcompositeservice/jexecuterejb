package fr.dauphine.message;

public enum ControlMessageType {
	COMPENSATE, COMPENSATE_TOKEN, CHECKPOINT, SKIP, TERMINATE
}
