package fr.dauphine.message;

public interface Message {
	
	public String getSender();
	public void setSender(String sender);

}
