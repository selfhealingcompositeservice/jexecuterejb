package fr.dauphine.message;

public class ControlMessage implements Message {
	
	private ControlMessageType message;
	private String sender;
	
	private double accumulatedTime;

	public ControlMessageType getMessage() {
		return message;
	}

	public void setMessage(ControlMessageType message) {
		this.message = message;
	}

	@Override
	public String getSender() {
		return sender;
	}

	@Override
	public void setSender(String sender) {
		this.sender = sender;	
	}

	public double getAccumulatedTime() {
		return accumulatedTime;
	}


	public void setAccumulatedTime(double accumulatedTime) {
		this.accumulatedTime = accumulatedTime;
	}

}
