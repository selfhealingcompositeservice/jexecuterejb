package fr.dauphine.engine;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.easyrules.core.AnnotatedRulesEngine;

import jersey.repackaged.com.google.common.base.Stopwatch;
import fr.dauphine.message.ControlMessage;
import fr.dauphine.message.ControlMessageType;
import fr.dauphine.message.DataMessage;
import fr.dauphine.message.Message;
import fr.dauphine.message.MessageFactory;
import fr.dauphine.service.Constants.EXECUTION_STATE;
import fr.dauphine.service.Data;
import fr.dauphine.service.QoS;
import fr.dauphine.service.Service;
import fr.dauphine.service.TransactionalProperty;
import fr.dauphine.engine.RunService;
import fr.dauphine.engine.kb.Analysis;
import fr.dauphine.engine.kb.AnalysisType;
import fr.dauphine.engine.kb.EngineThreadKB;
import fr.dauphine.engine.kb.Plans;
import fr.dauphine.engine.rules.PreconditionRule;

public class EngineThread implements Runnable, Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Service service;
	private Map<String, String> receivedInputs;
	private LinkedBlockingDeque<Message> queue;
	private MessageFactory messageFactory;
	private Collection<Service> successors;
	private Collection<Service> predecessors;
	private List<Service> substitutes;
	
	/**
	 * Config
	 */
	
	private ExecutionPreferences preferences;
	
	/**
	 * Stats
	 */
	private boolean failedUnique = false;
	private boolean failed = false;
	private double totalExecutionTime;
	private double totalPrice;
	private int retryNumber;
	private int substitutionNumber; 
	private List<Double> failureTimes;
	
	
	/**
	 * Knowledge Base
	 */
	
	private EngineThreadKB kb;
	
	private Stopwatch stopwatch;
	
	private double accumulatedTime;
	
	private AnnotatedRulesEngine rulesPrecondition;
	
	private static final Logger logger = Logger.getLogger("EngineThread");
	
	
	public EngineThread(Service service, Collection<Service> predecessors, 
			Collection<Service> successors, LinkedBlockingDeque<Message> queue, MessageFactory messageFactory,
			ExecutionPreferences preferences, EngineThreadContext context) {
		logger.log(Level.INFO, "Started Engine Thread: " + service.getName());
		this.service = service;
		this.queue = queue;
		this.successors = successors;
		this.predecessors = predecessors;
		this.messageFactory = messageFactory;
		receivedInputs = buildInputList();
		this.substitutes = new ArrayList<>();
		failureTimes = new ArrayList<>();
		
		this.preferences = preferences;
		
		 // create a rules engine
        rulesPrecondition =
                            new AnnotatedRulesEngine();
        //register the rule
        rulesPrecondition.registerRule(new PreconditionRule());
        
        //kb
        kb = new EngineThreadKB();
        kb.setName(this.service.getName());
        kb.setTransactionalProperty(this.service.getTransactionalProperty());
        QoS myQoS = new QoS();
        myQoS.setTime(service.getEstimatedExecutionTime());
        myQoS.setPrice(service.getPrice());
        myQoS.setAvailability(service.getAvailability());
        kb.setMyQoS(myQoS);
        kb.setContext(context);
        
        this.accumulatedTime = 0.0;
		
	}

	private Map<String, String> buildInputList() {
		Map<String, String> inputList = new HashMap<>();
		
		for(Data data:service.getInputs())
			inputList.put(data.getName(), null);
		
		return inputList;
	}

	public void run() {
		System.out.println("Running " + service.getName());
		
		stopwatch = new Stopwatch();
		stopwatch.start();
		
		service.setExecutionState(EXECUTION_STATE.INITIAL);
		
		while(!isFireable() && service.getExecutionState().equals(EXECUTION_STATE.INITIAL)) {
			waitMessage();
		}
		
		//if the SKIPPED message is among the received outputs,
		//the service must be SKIPPED
		if(receivedInputs.values().contains(ControlMessageType.SKIP.toString())) {
			service.setExecutionState(EXECUTION_STATE.SKIPPED);
			sendOutputs(true);
		}
		
		
		//it successfully received all its outputs
		if(service.getExecutionState().equals(EXECUTION_STATE.INITIAL)) {
			
			Plans plan = selfHealingTest(AnalysisType.WHEN_FIREABLE);
			
			logger.log(Level.INFO, "Engine Thread: " + service.getName() + " is fireable");
			
			try {
				execute(plan);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			if(!failed)
				sendOutputs();
		}
		
		try {
			executeFinalPhase();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	private void execute(Plans plan) throws InterruptedException {
		
		failed = !runService(plan);
		if(failed) {
			
			failedUnique = failed;
			recordFailure();
			if(preferences.isFaultTolerance()) {
				
				plan = selfHealingTest(AnalysisType.WHEN_FAILED);
				
				if(preferences.isSelfhealing() && plan.equals(Plans.COMPENSATE)) {
					compensate();
				} else if(service.getTransactionalProperty().equals(TransactionalProperty.PivotRetriable)
						|| service.getTransactionalProperty().equals(TransactionalProperty.CompensableRetriable)) {
					do {
						setRetryNumber(1);
						logger.log(Level.INFO, "Engine Thread: " + service.getName() + " retry");
						plan = Plans.CONTINUE;
						failed = !runService(plan);;
						if(failed) {
							recordFailure();
							plan = selfHealingTest(AnalysisType.WHEN_FAILED);
							if(preferences.isSelfhealing() && plan.equals(Plans.COMPENSATE)) {
								compensate();
								return;
							}
						} else
							selfHealingTest(AnalysisType.WHEN_REPAIRED);
					} while(failed);
					setTotalPrice(service.getPrice());
					service.setExecutionState(EXECUTION_STATE.EXECUTED);
				} else {
					while(substitutes.size() > 0 && failed) {
						logger.log(Level.INFO, "Engine Thread: " + service.getName() + " substitute");
						plan = Plans.CONTINUE;
						failed = !runService(plan);
					}
					if(failed) 
						compensate();
					else {
						setTotalPrice(service.getPrice());
						service.setExecutionState(EXECUTION_STATE.EXECUTED);
					}
				}
			} else {
				//non-fault tolerant execution. same procedure as checkpointing.
				service.setExecutionState(EXECUTION_STATE.FAILED);
				logger.log(Level.INFO, "Failed Engine Thread: " + service.getName() + " sends SKIP"  + " to "+ successors + " acct " + accumulatedTime);
				sendOutputs(true);
			}
		} else { //Execution success
			service.setExecutionState(EXECUTION_STATE.EXECUTED);
			setTotalPrice(service.getPrice());
		}
	}
	
	private void recordFailure() {
		double elapsedTime = (double) stopwatch.elapsed(java.util.concurrent.TimeUnit.MILLISECONDS);
		failureTimes.add(elapsedTime);
		logger.log(Level.INFO, "Engine Thread: " + service.getName() + " failed. availability=" + service.getAvailability() + " at " + elapsedTime + " ms.");
	}

	private Plans selfHealingTest(AnalysisType analysisType) {
		Plans plan = null;
		if(preferences.isSelfhealing()) {
			plan = this.kb.detect(analysisType);
		}
		return plan;
	}

	private boolean runService(Plans plan) {
		System.out.println("runService " + service.getName() + " " + plan);
		if(plan == null)
			plan = Plans.CONTINUE;
		if(plan.equals(Plans.CONTINUE)) {
			RunService rs = new RunService(service);
			
			Stopwatch stopwatch = new Stopwatch();
			stopwatch.start();
			rs.run();
			setTotalExecutionTime(stopwatch.elapsed(java.util.concurrent.TimeUnit.MILLISECONDS));
			this.accumulatedTime += rs.getExecutionTime();
			kb.updateLocalTime(rs.getExecutionTime());
			
			return rs.getSuccess();
		} else if(plan.equals(Plans.REPLICATE)) {
			ReplicationManager rm = new ReplicationManager(service);
			RunService rs = new RunService(rm.getReplicas());
			
			Stopwatch stopwatch = new Stopwatch();
			stopwatch.start();
			rs.run();
			setTotalExecutionTime(stopwatch.elapsed(java.util.concurrent.TimeUnit.MILLISECONDS));
			this.accumulatedTime += rs.getExecutionTime();
			kb.updateLocalTime(rs.getExecutionTime());
			
			return rs.getSuccess();
		} else if(plan.equals(Plans.COMPENSATE)) {
			RunService rs = new RunService(service);
			
			Stopwatch stopwatch = new Stopwatch();
			stopwatch.start();
			rs.run();
			setTotalExecutionTime(stopwatch.elapsed(java.util.concurrent.TimeUnit.MILLISECONDS));
			this.accumulatedTime += rs.getExecutionTime();
			return true;
		}
		
		return false;
	}

	private void compensate() throws InterruptedException {
		logger.log(Level.INFO, "Engine Thread: " + service.getName() + " compensate");
		
		if(failed) {
			messageFactory.sendToExecutionEngine(MessageFactory.getCompensateMessage());
			service.getExecutionState().equals(EXECUTION_STATE.ABANDONED);
		}
		
		if(service.getExecutionState().equals(EXECUTION_STATE.ABANDONED)
				|| service.getExecutionState().equals(EXECUTION_STATE.COMPENSATED)) {
			//No need to compensate send control tokens
			logger.log(Level.INFO, "Engine Thread: " + service.getName() + " no need to compensate (ABANDONED|COMPENSATED)");
			waitCompensate();
		} else {
			
			if(service.getExecutionState().equals(EXECUTION_STATE.INITIAL)) {
				//No need to compensate.
				logger.log(Level.INFO, "Engine Thread: " + service.getName() + " no need to compensate (INITIAL)");
				service.setExecutionState(EXECUTION_STATE.ABANDONED);
				waitCompensate();
			} else if(service.getExecutionState().equals(EXECUTION_STATE.EXECUTED)) {
				waitCompensate();
				Plans plan = Plans.COMPENSATE;
				runService(plan);
				service.setExecutionState(EXECUTION_STATE.COMPENSATED);
			}
		}
		//Send control tokens
		for(Service s:predecessors) {
			logger.info("Engine Thread " + service.getName() + " send compensation token to  " + s.getName());
			Message msg = MessageFactory.getCompensateTokenMessage();
			((ControlMessage) msg).setAccumulatedTime(accumulatedTime);
			msg.setSender(service.getName());
			if(s.getName().equals(fr.dauphine.service.Constants.INITIAL_NODE))
				messageFactory.send(msg, fr.dauphine.service.Constants.FINAL_NODE);
			else
				messageFactory.send(msg, s);
		}
		
	}
	
	private void waitCompensate() {
		//Wait until it becomes fireable
		logger.log(Level.INFO, "Engine Thread: " + service.getName() + " wait fireable for compensation");
		receivedInputs = new HashMap<>();
		for(Service s:successors)
			receivedInputs.put(s.getName(), null);
		
		while(receivedInputs.containsValue(null)) {
			Message msg;
			try {
				msg = queue.take();
				if(msg instanceof ControlMessage && ((ControlMessage)msg).getMessage().equals(ControlMessageType.COMPENSATE_TOKEN)) {
					updateAccumulatedTime(((ControlMessage)msg).getAccumulatedTime());
					receivedInputs.put(((ControlMessage)msg).getSender(), ControlMessageType.COMPENSATE_TOKEN.toString());
				} else
					logger.severe("Engine Thread in waitCompensate receiver " + msg);
					
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}

	private void sendOutputs() {
		sendOutputs(false);
	}
	
	/**
	 * Sends outputs to successors.
	 * If isCheckpointing=true, it sends assigns the value SKIP to each output token.
	 * isCheckpointing=true for failed and skipped services during checkpointing
	 * and during non-fault tolerant executions.
	 * @param isCheckpoint
	 */
	private void sendOutputs(boolean isCheckpoint) {
		List<Message> outputs = new ArrayList<>();
		//assign a value to each output token
		for(Data d:service.getOutputs()) {
			String dataValue = "";
			if(isCheckpoint)
				dataValue = ControlMessageType.SKIP.toString();
			else //normal, successful execution
				dataValue = d.getName();
			DataMessage dm = new DataMessage(d, dataValue);
			dm.setAccumulatedTime(accumulatedTime);
			logger.log(Level.INFO, "Engine Thread: " + service.getName() + " sendsAcc" + accumulatedTime);
			outputs.add(dm);
		}
		logger.log(Level.INFO, "Engine Thread: " + service.getName() + " sends " + outputs  + " to "+ successors);
		messageFactory.send(outputs, successors);
	}

	private void waitMessage() {
		try {
			Message msg = queue.take();
			logger.log(Level.INFO, "Engine Thread: " + service.getName() + " received " + msg);
			if(msg instanceof ControlMessage) {
				//compensation stops to unrolling of the composition
				ControlMessage cmsg = (ControlMessage) msg;
				if(cmsg.getMessage().equals(ControlMessageType.COMPENSATE))
					compensate();
			} else if(msg instanceof DataMessage) {
				//normal and checkpointing are the same since
				//the composition unrolling does not change
				DataMessage dmsg = (DataMessage) msg;
				updateAccumulatedTime(dmsg.getAccumulatedTime());
				receivedInputs.put(dmsg.getData().getName(), dmsg.getValue());
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void updateAccumulatedTime(double accumulatedTime) {
		this.accumulatedTime = accumulatedTime > this.accumulatedTime ? accumulatedTime:this.accumulatedTime;
		kb.updateAccumulatedTime(this.accumulatedTime);
	}

	private boolean isFireable() {
		return !receivedInputs.containsValue(null);
	}

	private void executeFinalPhase() throws Exception {
		//Final phase. Wait for an instruction
		while(true) {
			logger.info("Final Phase Engine Thread " + service.getName());
			Message msg = queue.take();
			if(msg instanceof ControlMessage){
				ControlMessage cmsg = (ControlMessage) msg;
				if(cmsg.getMessage().equals(ControlMessageType.COMPENSATE))
					compensate();
				if(cmsg.getMessage().equals(ControlMessageType.TERMINATE)) {
					logger.info("Terminate signal received Engine Thread " + service.getName());
					return;
				}
			} else
				logger.severe("Final Phase Execution Engine: " + "unknown message");

		}
	}

	public double getTotalExecutionTime() {
		return totalExecutionTime;
	}

	private void setTotalExecutionTime(double totalExecutionTime) {
		this.totalExecutionTime += totalExecutionTime;
	}
	

	public double getTotalPrice() {
		return totalPrice;
	}

	private void setTotalPrice(double totalPrice) {
		this.totalPrice += totalPrice;
	}

	public String getName() {
		
		return service.getName();
	}
	
	public EXECUTION_STATE getState() {
		return service.getExecutionState();
	}

	public int getRetryNumber() {
		return retryNumber;
	}

	private void setRetryNumber(int retryNumber) {
		this.retryNumber += retryNumber;
	}
	
	public boolean isFailed() {

		return failedUnique;
	}

	public List<Double> getFailureTimes() {
		return failureTimes;
	}

	public int getSubstitutionNumber() {
		return substitutionNumber;
	}
	
	public double getAccumulatedTime() {
		return this.accumulatedTime;
	}

	public List<Analysis> getAnalysis() {
		return kb.getAnalysis();
	}
}
