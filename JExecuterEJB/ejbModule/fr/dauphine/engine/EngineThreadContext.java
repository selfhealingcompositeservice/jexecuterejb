package fr.dauphine.engine;

import java.util.Set;

import fr.dauphine.graphanalyser.TimeContext;
import fr.dauphine.service.Data;

public class EngineThreadContext {
	
	private Set<String> allPredecessors;
	private Set<Data> outputDependency;
	private int servicesNumber;
	private int totalOutputsNumber;
	private TimeContext timeContext;

	public EngineThreadContext(Set<String> allPredecessors, Set<Data> outputDependency,
			int servicesNumber, int totalOutputsNumber, TimeContext timeContext) {
		this.allPredecessors = allPredecessors;
		this.outputDependency = outputDependency;
		this.servicesNumber = servicesNumber;
		this.totalOutputsNumber = totalOutputsNumber;
		this.timeContext = timeContext;
	}

	public Set<String> getAllPredecessors() {
		return allPredecessors;
	}

	public Set<Data> getOutputDependency() {
		return outputDependency;
	}

	public int getServicesNumber() {
		return servicesNumber;
	}

	public int getTotalOutputsNumber() {
		return totalOutputsNumber;
	}
	
	public TimeContext getTimeContext() {
		return this.timeContext;
	}
	
}
