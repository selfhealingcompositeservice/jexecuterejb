package fr.dauphine.engine.kb;

import fr.dauphine.service.TransactionalProperty;

/**
 * Record detection and diagnosis
 * Service name
 * Time of analysis
 * Work Done: Predecessors, Output Dependency, Weights, Threshold
 * Time: localExpectedTime, localRemainingTime, localTime, freeTime, passedTime, exceedingPercentage, Threshold,
 *       replicationActive, 
 * Diagnostic: Enum value in class Plans, Normal decision without selfhealing
 * Price: ???
 **/
public class Analysis {
	
	private String serviceName;
	private Plans recoveryPlan;
	private Plans defaultRecoveryPlan;
	
	private AnalysisType analysisType;
	private SelfhealingStates previousSelfhealingState;
	private SelfhealingStates selfhealingState;
	private TransactionalProperty transactionalProperty;
	
	//the time when this analysis was performed
	private double analysisTime;
	
	//work done
	private double workDone;
	private double outputDependency;
	private double allPredecessors;
	private double weightAllPredecessors;
	private double weightOutputDependency;
	private double workDoneThreshold;
	
	//time
	private double localExpectedTime;
	private double localRemainingTime;
	private double globalExpectedTime;
	private double freeTime;
	private double estimatedExecutionTime;
	private double actualConsumedTime;
	private double updatedFreeTime;
	private double updatedGlobalExpectedTime;
	private double exceedingPercentage;
	private double updatedExceedingPercentage;
	private double timeThreshold;
	private double localCriticalPath;
	
	/**
	 * 
	 * @return
	 */
	public String getServiceName() {
		return serviceName;
	}
	
	/**
	 * 
	 * @param serviceName
	 */
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	
	/**
	 * 
	 * @return
	 */
	public Plans getRecoveryPlan() {
		return recoveryPlan;
	}
	
	/**
	 * 
	 * @param recoveryPlan
	 */
	public void setRecoveryPlan(Plans recoveryPlan) {
		this.recoveryPlan = recoveryPlan;
	}
	
	/**
	 * 
	 * @return
	 */
	public double getAnalysisTime() {
		return analysisTime;
	}
	
	/**
	 * 
	 * @param analysisTime
	 */
	public void setAnalysisTime(double analysisTime) {
		this.analysisTime = analysisTime;
	}
	
	/**
	 * 
	 * @return
	 */
	public double getWorkDone() {
		return workDone;
	}
	
	/**
	 * 
	 * @param workDone
	 */
	public void setWorkDone(double workDone) {
		this.workDone = workDone;
	}
	
	/**
	 * 
	 * @return
	 */
	public double getOutputDependency() {
		return outputDependency;
	}
	
	/**
	 * 
	 * @param outputDependency
	 */
	public void setOutputDependency(double outputDependency) {
		this.outputDependency = outputDependency;
	}
	
	/**
	 * 
	 * @return
	 */
	public double getAllPredecessors() {
		return allPredecessors;
	}
	
	/**
	 * 
	 * @param allPredecessors
	 */
	public void setAllPredecessors(double allPredecessors) {
		this.allPredecessors = allPredecessors;
	}
	
	/**
	 * 
	 * @return
	 */
	public double getWeightAllPredecessors() {
		return weightAllPredecessors;
	}
	
	/**
	 * 
	 * @param weightAllPredecessors
	 */
	public void setWeightAllPredecessors(double weightAllPredecessors) {
		this.weightAllPredecessors = weightAllPredecessors;
	}
	
	/**
	 * 
	 * @return
	 */
	public double getWeightOutputDependency() {
		return weightOutputDependency;
	}
	
	/**
	 * 
	 * @param weightOutputDependency
	 */
	public void setWeightOutputDependency(double weightOutputDependency) {
		this.weightOutputDependency = weightOutputDependency;
	}
	
	/**
	 * 
	 * @return
	 */
	public double getWorkDoneThreshold() {
		return workDoneThreshold;
	}
	
	/**
	 * 
	 * @param workDoneThreshold
	 */
	public void setWorkDoneThreshold(double workDoneThreshold) {
		this.workDoneThreshold = workDoneThreshold;
	}
	
	/**
	 * 
	 * @return
	 */
	public double getLocalExpectedTime() {
		return localExpectedTime;
	}
	
	/**
	 * 
	 * @param localExpectedTime
	 */
	public void setLocalExpectedTime(double localExpectedTime) {
		this.localExpectedTime = localExpectedTime;
	}
	
	/**
	 * 
	 * @return
	 */
	public double getLocalRemainingTime() {
		return localRemainingTime;
	}
	
	/**
	 * 
	 * @param localRemainingTime
	 */
	public void setLocalRemainingTime(double localRemainingTime) {
		this.localRemainingTime = localRemainingTime;
	}
	
	/**
	 * Returns the composition estimated execution time
	 * @return
	 */
	public double getGlobalExpectedTime() {
		return globalExpectedTime;
	}
	
	/**
	 * Sets the composition estimated execution time
	 * @param globalExpectedTime
	 */
	public void setGlobalExpectedTime(double globalExpectedTime) {
		this.globalExpectedTime = globalExpectedTime;
	}
	
	/**
	 * Returns the free time at the moment of analysis
	 * @return
	 */
	public double getFreeTime() {
		return freeTime;
	}
	
	/**
	 * Sets the free time at the moment of analysis
	 * @param freeTime
	 */
	public void setFreeTime(double freeTime) {
		this.freeTime = freeTime;
	}
	/**
	 * Returns the estimated execution time of the analyzed service
	 * @return
	 */
	public double getEstimatedExecutionTime() {
		return estimatedExecutionTime;
	}
	/**
	 * Sets the estimated execution time of the analyzed service
	 * @param estimatedExecutionTime
	 */
	public void setEstimatedExecutionTime(double estimatedExecutionTime) {
		this.estimatedExecutionTime = estimatedExecutionTime;
	}
	
	/**
	 * Returns the consumed time at the moment of the analysis:
	 * Function:
	 * expectedTime + localTime
	 * @return
	 */
	public double getActualConsumedTime() {
		return actualConsumedTime;
	}
	
	/**
	 * Sets the consumed time at the moment of the analysis:
	 * Function:
	 * expectedTime + localTime
	 * @param actualConsumedTime
	 */
	public void setActualConsumedTime(double actualConsumedTime) {
		this.actualConsumedTime = actualConsumedTime;
	}
	
	/**
	 * Returns the updated free time after the analysis of the current situation.
	 * @return
	 */
	public double getUpdatedFreeTime() {
		return updatedFreeTime;
	}
	
	/**
	 * Sets the updated free time after the analysis of the current situation.
	 * @param updatedFreeTime
	 */
	public void setUpdatedFreeTime(double updatedFreeTime) {
		this.updatedFreeTime = updatedFreeTime;
	}
	
	/**
	 * Returns the updated composition estimated time after the analysis of the current situation.
	 * Function: 
	 * ExpectedTime + localTime + EstimatedExecutionTime + RemainingTime
	 * @return
	 */
	public double getUpdatedGlobalExpectedTime() {
		return updatedGlobalExpectedTime;
	}
	/**
	 * Sets the updated composition estimated time after the analysis of the current situation.
	 * Function: 
	 * ExpectedTime + localTime + EstimatedExecutionTime + RemainingTime
	 * @param updatedGlobalExpectedTime
	 */
	public void setUpdatedGlobalExpectedTime(double updatedGlobalExpectedTime) {
		this.updatedGlobalExpectedTime = updatedGlobalExpectedTime;
	}
	
	/**
	 * Returns the percentage regarding the original composition estimated time.
	 * For services in the Critical Path, it should be close to 0.
	 * For other services, it can have a negative value.
	 *  @return
	 */
	public double getExceedingPercentage() {
		return exceedingPercentage;
	}
	/**
	 * Sets the percentage regarding the original composition estimated time.
	 * For services in the Critical Path, it should be close to 0.
	 * For other services, it can have a negative value.
	 * @param exceedingPercentage
	 */
	public void setExceedingPercentage(double exceedingPercentage) {
		this.exceedingPercentage = exceedingPercentage;
	}
	
	/**
	 * Returns the exceeding percentage regarding the original composition estimated time
	 * after the current situation was analyzed.
	 * Function:
	 * ((updatedCompositionEET-compositionEET)*100)/compositionEET
	 *  @return
	 */
	public double getUpdatedExceedingPercentage() {
		return updatedExceedingPercentage;
	}
	
	/**
	 * Sets the exceeding percentage regarding the original composition estimated time
	 * after the current situation was analyzed.
	 * Function:
	 * ((updatedCompositionEET-compositionEET)*100)/compositionEET
	 * @param updatedExceedingPercentage
	 */
	public void setUpdatedExceedingPercentage(double updatedExceedingPercentage) {
		this.updatedExceedingPercentage = updatedExceedingPercentage;
	}

	public SelfhealingStates getSelfhealingState() {
		return selfhealingState;
	}

	public void setSelfhealingState(SelfhealingStates selfhealingState) {
		this.selfhealingState = selfhealingState;
	}

	public double getTimeThreshold() {
		return timeThreshold;
	}

	public void setTimeThreshold(double timeThreshold) {
		this.timeThreshold = timeThreshold;
	}

	public TransactionalProperty getTransactionalProperty() {
		return transactionalProperty;
	}

	public void setTransactionalProperty(TransactionalProperty transactionalProperty) {
		this.transactionalProperty = transactionalProperty;
	}

	/**
	 * Returns the self-healing state previous to the current analysis
	 * @return the previous self-healing state
	 */
	public SelfhealingStates getPreviousSelfhealingState() {
		return previousSelfhealingState;
	}

	/**
	 * Sets the self-healing state previous to the current analysis
	 * @param previousSelfhealingState
	 */
	public void setPreviousSelfhealingState(SelfhealingStates previousSelfhealingState) {
		this.previousSelfhealingState = previousSelfhealingState;
	}

	public AnalysisType getAnalysisType() {
		return analysisType;
	}

	public void setAnalysisType(AnalysisType analysisType) {
		this.analysisType = analysisType;
	}

	public double getLocalCriticalPath() {
		return localCriticalPath;
	}

	public void setLocalCriticalPath(double localCriticalPath) {
		this.localCriticalPath = localCriticalPath;
	}

	public Plans getDefaultRecoveryPlan() {
		return defaultRecoveryPlan;
	}

	public void setDefaultRecoveryPlan(Plans defaultRecoveryPlan) {
		this.defaultRecoveryPlan = defaultRecoveryPlan;
	}

}
