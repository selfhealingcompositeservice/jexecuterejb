package fr.dauphine.engine.kb;

public enum Constants {

	SELF_HEALING("self"),
	TIME("time");

    private final String textRepresentation;

    private Constants(String textRepresentation) {
        this.textRepresentation = textRepresentation;
    }

    @Override public String toString() {
         return textRepresentation;
    }
}
