package fr.dauphine.engine.kb;

import java.util.ArrayList;
import java.util.List;

import jersey.repackaged.com.google.common.base.Stopwatch;
import fr.dauphine.engine.EngineThreadContext;
import fr.dauphine.graphanalyser.TimeContext;
import fr.dauphine.service.QoS;
import fr.dauphine.service.TransactionalProperty;

public class EngineThreadKB {
	
	/* 
	 * Constants
	 */
	final static Double THRESHOLD_TIME = 5.0;
	final static Double THRESHOLD_PROGRESS = 70.0;
	final static Double THRESHOLD_AVAILABILITY = 0.5;
	/*
	 * Attributes
	 */
	
	private String name;
	private QoS myQoS;
	private TransactionalProperty transactionalProperty;
	private EngineThreadContext context;
	private double localTime;
	private double accumulatedTime;
	private SelfhealingStates currentSelsSelfhealingState;
	private AnalysisType analysisType;
	private boolean canCompensate;
	
	/*
	 * Analysis
	 */
	private Stopwatch stopwatch;
	
	private List<Analysis> analysis;
	
	public EngineThreadKB() {
		
		this.currentSelsSelfhealingState = SelfhealingStates.NORMAL;
		
		this.localTime = 0.0;
		
		this.canCompensate = true;
		
		stopwatch = new Stopwatch();
		stopwatch.start();
		
		analysis = new ArrayList<>();
	}
	
	public QoS getMyQoS() {
		return myQoS;
	}
	public void setMyQoS(QoS myQoS) {
		this.myQoS = myQoS;
	}

	public void setContext(EngineThreadContext context) {
		this.context = context;
	}
	
	public EngineThreadContext getContext() {
		return context;
	}
	
	
	public Plans detect(AnalysisType analysisType) {
		Analysis analysis = new Analysis();
		
		this.analysisType = analysisType;
		
		

		SelfhealingStates state = getTimeAnalysis(analysis, analysisType);
		
		if(analysisType.equals(AnalysisType.WHEN_FAILED) && state.equals(SelfhealingStates.NORMAL))
			state = SelfhealingStates.DEGRADED;
		
		Plans plan = getPlan(state, analysisType, analysis);
		
		
		
		analysis.setRecoveryPlan(plan);
		
		
		//set default recovery plan
		if(analysisType.equals(AnalysisType.WHEN_FAILED) || plan.equals(Plans.COMPENSATE)) {
			if(this.transactionalProperty.equals(TransactionalProperty.CompensableRetriable) ||
					this.transactionalProperty.equals(TransactionalProperty.PivotRetriable))
					analysis.setDefaultRecoveryPlan(Plans.RETRY);
			else
				analysis.setDefaultRecoveryPlan(Plans.COMPENSATE);
		} else
			analysis.setDefaultRecoveryPlan(null);
		
		System.out.println("name=" + this.name + ", analysisType=" + analysisType + ", previousState=" +analysis.getPreviousSelfhealingState() 
				+ ", state=" + analysis.getSelfhealingState() + ", recoveryPlan=" + analysis.getRecoveryPlan() + ", defaultRecoveryPlan=" + analysis.getDefaultRecoveryPlan());
		this.analysis.add(analysis);
		return plan;
	}
	
	private Plans getPlan(SelfhealingStates state, AnalysisType analysisType, Analysis analysis) {
		Plans plan = null;
		
		//Broken State Rules
		if(state.equals(SelfhealingStates.BROKEN)) {
			
			plan = fireBrokenRules();
		//Degraded State Rules
		} else if(state.equals(SelfhealingStates.DEGRADED)) {
			if(analysisType.equals(AnalysisType.WHEN_FAILED)) {
				plan = fireDegradedRules(analysis);
			} else {
				plan = fireNormalRules(analysis);
			}
				
		} else if(state.equals(SelfhealingStates.NORMAL)) {
			plan = fireNormalRules(analysis);
		}
		
		return plan;
	}
	
	private Plans fireNormalRules(Analysis analysis) {
		Plans plan = null;
		if(this.myQoS.getAvailability() < THRESHOLD_AVAILABILITY && !this.analysisType.equals(AnalysisType.WHEN_REPAIRED))
			plan = Plans.REPLICATE;
		else if(analysis.getUpdatedFreeTime() < this.myQoS.getTime() && !this.analysisType.equals(AnalysisType.WHEN_REPAIRED)) //no time to afford another failure
			plan = Plans.REPLICATE;
		else
			plan = Plans.CONTINUE;
		return plan;
	}
	
	private Plans fireDegradedRules(Analysis analysis) {
		Plans plan = null;
		//if it is not retiable
		if(!this.transactionalProperty.equals(TransactionalProperty.CompensableRetriable) &&
				!this.transactionalProperty.equals(TransactionalProperty.PivotRetriable)) {
			plan = Plans.COMPENSATE;
			
		} else {
			plan =  fireNormalRules(analysis);
			if(plan.equals(Plans.CONTINUE))
				plan = Plans.RETRY;
		}
		return plan;
	}
	
	private Plans fireBrokenRules() {
		Plans plan = null;
		if(!this.transactionalProperty.equals(TransactionalProperty.CompensableRetriable) &&
				!this.transactionalProperty.equals(TransactionalProperty.PivotRetriable)) {
			plan = Plans.COMPENSATE;
			
		} else {
			if(!canCompensate) {
				plan = Plans.RETRY;
			} else {
				if(getWorkDone() > THRESHOLD_PROGRESS) {
					plan = Plans.RETRY;
				} else {
					plan = Plans.COMPENSATE;
				}
			}
		}
		return plan;
	}

	private SelfhealingStates getTimeAnalysis(Analysis analysis, AnalysisType analysisType) {
		double elapsedTime = (double) stopwatch.elapsed(java.util.concurrent.TimeUnit.MILLISECONDS);
		TimeContext timeContext = context.getTimeContext();
		
		if(timeContext.getFreeTime() < 0 )
			System.out.println("There was already no free time for " + name);
		
		//System.out.println("Time analysis + " + name + " localExpectedTime=" + timeContext.getExpectedTime()
		//		+ " localRemainingTime=" + timeContext.getRemainingTime() + " globalExpectedTime=" + timeContext.getGlobalEstimatedTime()
		//		+ " localTime=" + localTime + " freetime=" + timeContext.getFreeTime());
		
		double localCriticalPathExpectedTime = timeContext.getExpectedTime() + timeContext.getEstimatedExecutionTime() +  timeContext.getRemainingTime();
		double estimatedTime =  timeContext.getEstimatedExecutionTime();
		if(analysisType.equals(AnalysisType.WHEN_REPAIRED))
			estimatedTime = 0;
		double updatedLocalCriticalPathExpectedTime = getUpdatedGlobalEstimatedExecutionTime(this.accumulatedTime, localTime, estimatedTime, timeContext.getRemainingTime()) ;
		double updatedFreeTime = getUpdatedFreeTime(timeContext.getGlobalEstimatedTime(), updatedLocalCriticalPathExpectedTime);
		
		//System.out.println("Time analysis2 " + updatedFreeTime + " newGlobalExpectedTime=" + updatedGlobalExpectedTime 
		//		+ "(" + getExceedingPercentageTime(updatedGlobalExpectedTime, timeContext.getGlobalEstimatedTime()) + " %)");
		//System.out.println("Time analysis3 " + accumulatedTime + " - ex " + timeContext.getExpectedTime());
		double exceedingPercentage = getExceedingPercentageTime(updatedLocalCriticalPathExpectedTime, timeContext.getGlobalEstimatedTime());
		
		//self-healing state
		SelfhealingStates previousSelfhealingState = this.currentSelsSelfhealingState;
		if(updatedLocalCriticalPathExpectedTime > localCriticalPathExpectedTime) {
			if(exceedingPercentage > THRESHOLD_TIME && !analysisType.equals(AnalysisType.WHEN_REPAIRED)) //Timeout(QoS)
				this.currentSelsSelfhealingState = SelfhealingStates.BROKEN;
			else { //degraded
				if(this.analysisType.equals(AnalysisType.WHEN_REPAIRED))
					if(exceedingPercentage <=0)
						this.currentSelsSelfhealingState = SelfhealingStates.NORMAL;
					else
						this.currentSelsSelfhealingState = SelfhealingStates.DEGRADED;
				else
					this.currentSelsSelfhealingState = SelfhealingStates.DEGRADED;
			}
		}
		
		analysis.setServiceName(name);
		analysis.setAnalysisType(this.analysisType);
		analysis.setActualConsumedTime(this.accumulatedTime + localTime);
		analysis.setTransactionalProperty(transactionalProperty);
		analysis.setPreviousSelfhealingState(previousSelfhealingState);
		analysis.setSelfhealingState(this.currentSelsSelfhealingState);
		analysis.setUpdatedGlobalExpectedTime(updatedLocalCriticalPathExpectedTime);
		analysis.setGlobalExpectedTime(timeContext.getGlobalEstimatedTime());
		analysis.setTimeThreshold(THRESHOLD_TIME);
		analysis.setAnalysisTime(elapsedTime);
		analysis.setExceedingPercentage(exceedingPercentage);
		analysis.setLocalExpectedTime(timeContext.getExpectedTime());
		analysis.setLocalRemainingTime(timeContext.getRemainingTime());
		analysis.setLocalCriticalPath(localCriticalPathExpectedTime);
		analysis.setUpdatedFreeTime(updatedFreeTime);
		
		System.out.println("Time analysis + " + name + " state " + this.currentSelsSelfhealingState + " updatedGlobalExpectedTime " + updatedLocalCriticalPathExpectedTime + " globalExpectedTime" + timeContext.getGlobalEstimatedTime() + " Tp=" + transactionalProperty);
		return this.currentSelsSelfhealingState;
	}
	
	/*
	 * The functions
	 */
	
	/**
	 * Returns the updated passed time.
	 * It is calculated by summing up the time passed before the execution
	 * of the current service and the local time consumed during its
	 * execution
	 * @param expectedTime
	 * @param localTime
	 * @return
	 */
	private double getUpdatedPassedTime(double expectedTimeAccumulated, double localTime) {
		return expectedTimeAccumulated + localTime;
	}
	
	/**
	 * Returns the updated free time
	 * @return
	 */
	private double getUpdatedFreeTime(double globalExpectedTime, double updatedGlobalExpectedTime) {
		return globalExpectedTime - updatedGlobalExpectedTime;
	}
	
	/**
	 * Returns the updated global estimated execution time
	 * @param expectedTime
	 * @param localTime
	 * @param estimatedExecutionTime
	 * @param remainingTime
	 * @return
	 */
	private double getUpdatedGlobalEstimatedExecutionTime(double expectedTimeAccumulated, double localTime,
			double estimatedExecutionTime, double remainingTime) {
		System.out.println("updatedGlobalExpectedTime name=" + this.name + ", expectedTimeAccumulated=" + expectedTimeAccumulated
				+ ", localTime=" + localTime + ", estimatedExecutionTime=" + estimatedExecutionTime + ", remainingTime=" + remainingTime);
		return expectedTimeAccumulated + localTime + estimatedExecutionTime + remainingTime;
	}
	
	
	private double getExceedingPercentageTime(double updatedGlobalEstimatedExecutionTime, double globalEstimatedExecutionTime) {
		return ((updatedGlobalEstimatedExecutionTime-globalEstimatedExecutionTime)*100)/globalEstimatedExecutionTime;
	}
	
	/*
	 * End of the functions
	 */

	private double getWorkDone() {
		double wOd = 0.5;
		double wPred = 0.5;
		double workDone = (100 - getOutputDependencyPercentage())*wOd + getAllPredecessorsPercentage()*wPred;
		System.out.println("Work done + " + name + " " + workDone);
		return 0.0;
	}
	
	private double getOutputDependencyPercentage() {
		return (context.getOutputDependency().size()*100)/context.getTotalOutputsNumber();
	}
	
	private double getAllPredecessorsPercentage() {
		return (context.getAllPredecessors().size()*100)/context.getServicesNumber();
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public double getLocalTime() {
		return localTime;
	}

	public void updateLocalTime(double localTime) {
		this.localTime += localTime;
	}
	
	public void updateAccumulatedTime(double accumulatedTime) {
		this.accumulatedTime = accumulatedTime;
	}

	public TransactionalProperty getTransactionalProperty() {
		return transactionalProperty;
	}

	public void setTransactionalProperty(TransactionalProperty transactionalProperty) {
		this.transactionalProperty = transactionalProperty;
	}

	public List<Analysis> getAnalysis() {
		return analysis;
	}

	public boolean isCanCompensate() {
		return canCompensate;
	}

	public void setCanCompensate(boolean canCompensate) {
		this.canCompensate = canCompensate;
	}

}
