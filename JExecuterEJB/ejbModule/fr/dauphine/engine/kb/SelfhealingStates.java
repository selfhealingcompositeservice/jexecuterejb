package fr.dauphine.engine.kb;

public enum SelfhealingStates {
	
	NORMAL, DEGRADED, BROKEN

}
