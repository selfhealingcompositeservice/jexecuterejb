package fr.dauphine.engine.kb;

public enum Plans {
	REPLICATE, COMPENSATE, CHECKPOINT, CONTINUE, RETRY
}
