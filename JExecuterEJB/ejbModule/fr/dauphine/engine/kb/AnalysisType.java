package fr.dauphine.engine.kb;

public enum AnalysisType {

	/**
	 * Engine Threads can monitor what is going on when waiting for inputs, so it can
	 * start thinking about what to do
	 */
	WHEN_WAITING_FIREABLE,
	/**
	 * analysis just before executing its corresponding service
	 */
	WHEN_FIREABLE,
	/**
	 * analysis when execution. For example, to detect timeouts
	 */
	WHEN_EXECUTING,
	/**
	 * analysis when failed execution.
	 */
	WHEN_FAILED,
	/**
	 * analysis when repaired
	 */
	WHEN_REPAIRED;
}
