package fr.dauphine.engine.rules;

import org.easyrules.annotation.Action;
import org.easyrules.annotation.Condition;
import org.easyrules.annotation.Rule;
import org.easyrules.core.BasicRule;

@Rule(name = "Engine Thread preconditions" )
public class PreconditionRule extends BasicRule {
	
	
	
	@Condition
	//check current vs expected values
	public boolean check() {
		 return true;
	}
	
	@Action
	//diagnose
    public void diagnose() {
        System.out.println("Easy Rules rocks!");
    }

	
}
