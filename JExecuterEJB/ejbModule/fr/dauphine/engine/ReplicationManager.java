package fr.dauphine.engine;

import java.util.ArrayList;
import java.util.List;

import fr.dauphine.service.Service;

public class ReplicationManager {

	final static int REPLICA_NUMBER = 2;
	final static double DECREASE_AVAILABILITY = 0.03;
	private Service service;
	
	public ReplicationManager(Service service) {
		this.service = service;
	}

	/**
	 * Returns a list containing the original service and its replicas
	 * @return
	 */
	public List<Service> getReplicas() {
		List<Service> replicas = new ArrayList<>();
		replicas.add(service);
		
		double n = DECREASE_AVAILABILITY;
		for(int i=0; i<= REPLICA_NUMBER; i++) {
			Service r = new Service(service.getName() + "_r" + i);
			r.setAvailability(service.getAvailability() - n);
			n += DECREASE_AVAILABILITY;
		}
		return replicas;
	}

}
