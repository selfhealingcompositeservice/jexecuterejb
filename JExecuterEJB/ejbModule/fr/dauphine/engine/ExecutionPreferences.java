package fr.dauphine.engine;

public class ExecutionPreferences {
	
	private boolean isFaultTolerance; 
	private boolean isSelfhealing;
	
	public boolean isFaultTolerance() {
		return isFaultTolerance;
	}
	public void setFaultTolerance(boolean isFaultTolerance) {
		this.isFaultTolerance = isFaultTolerance;
	}
	public boolean isSelfhealing() {
		return isSelfhealing;
	}
	public void setSelfhealing(boolean isSelfhealing) {
		this.isSelfhealing = isSelfhealing;
	}
}
