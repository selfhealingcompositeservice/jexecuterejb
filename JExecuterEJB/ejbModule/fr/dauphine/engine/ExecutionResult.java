package fr.dauphine.engine;

import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import fr.dauphine.engine.kb.Analysis;
import fr.dauphine.service.CompositeTransactionalProperty;

public class ExecutionResult {
	
	//number of vertices
	private int serviceCount;
	
	//transactional property
	private CompositeTransactionalProperty transactionalProperty;
	
	//time taken to the Execution Engine implementation to do the execution
	private double executionEngineTime; 
	
	//estimated values
	private double estimatedExecutionTime;
	private double estimatedPrice;
	private double estimatedAvailability;
	
	//real QoS
	private double price;
	private double executionTime;
	
	//techniques
	private boolean success; //delivered all the outouts or no
	private boolean retry;
	private boolean substitution;
	private boolean compensation;
	private boolean checkpointing;
	
	//number of failures, substitutions, etc
	private int failureNumber; //number of total failures. 
	private int failureServiceNumber; //number of different services that failed
	private int retryNumber; //total number of retries
	private int retryNumberServices; //number of engine thread that used retry
	private int substitutionNumber; //total number of substitutions, including substitutions of substitutes.
	private int substitutionNumberServices; // number of engine threads that used substitution
	private int compensationNumber; //number of compensated services
	private int successNumber; //number of services executed successfully
	private int checkpointNumber; //number of checkpointed services
	
	//time of failures by services
	private java.util.Map<String, List<Double>> failureTimesByService;

	//retries by service
	private Map<String,Integer> retryNumberByService;
	//substitution by service
	private Map<String,Integer> substitutionNumberByService;
	
	//list
	private List<String> failedServices;
	private List<String> successfulServices;
	private List<String> producedOutputs;
	private List<String> compensatedServices;

	private double accumulatedTime;
	
	//execution time when received COMPENSATE message
	private double compensateMessageReceivedAt;
	
	//self healing
	private List<Analysis> selfHealingAnalysis;
	
	private static final Logger logger = Logger.getLogger(ExecutionResult.class.getCanonicalName());
	
	
	public double getEstimatedExecutionTime() {
		return estimatedExecutionTime;
	}
	public void setEstimatedExecutionTime(double estimatedExecutionTime) {
		this.estimatedExecutionTime = estimatedExecutionTime;
	}
	public double getEstimatedPrice() {
		return estimatedPrice;
	}
	public void setEstimatedPrice(double estimatedPrice) {
		this.estimatedPrice = estimatedPrice;
	}
	public double getEstimatedAvailability() {
		return estimatedAvailability;
	}
	public void setEstimatedAvailability(double estimatedAvailability) {
		this.estimatedAvailability = estimatedAvailability;
	}
	
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public boolean isRetry() {
		return retry;
	}
	public void setRetry(boolean retry) {
		this.retry = retry;
	}
	public boolean isSubstitution() {
		return substitution;
	}
	public void setSubstitution(boolean substitution) {
		this.substitution = substitution;
	}
	public boolean isCompensation() {
		return compensation;
	}
	public void setCompensation(boolean compensation) {
		this.compensation = compensation;
	}
	public boolean isCheckpointing() {
		return checkpointing;
	}
	public void setCheckpointing(boolean checkpointing) {
		this.checkpointing = checkpointing;
	}
	public double getExecutionEngineTime() {
		return executionEngineTime;
	}
	public void setExecutionEngineTime(double executionEngineTime) {
		this.executionEngineTime = executionEngineTime;
	}
	public int getFailureNumber() {
		return failureNumber;
	}
	public void setFailureNumber(int failureNumber) {
		this.failureNumber = failureNumber;
	}
	public int getFailureServiceNumber() {
		return failureServiceNumber;
	}
	public void setFailureServiceNumber(int failureServiceNumber) {
		this.failureServiceNumber = failureServiceNumber;
	}
	public int getRetryNumber() {
		return retryNumber;
	}
	public void setRetryNumber(int retryNumber) {
		this.retryNumber = retryNumber;
	}
	public int getRetryNumberServices() {
		return retryNumberServices;
	}
	public void setRetryNumberServices(int retryNumberServices) {
		this.retryNumberServices = retryNumberServices;
	}
	public int getSubstitutionNumber() {
		return substitutionNumber;
	}
	public void setSubstitutionNumber(int substitutionNumber) {
		this.substitutionNumber = substitutionNumber;
	}
	public int getSubstitutionNumberServices() {
		return substitutionNumberServices;
	}
	public void setSubstitutionNumberServices(int substitutionNumberServices) {
		this.substitutionNumberServices = substitutionNumberServices;
	}
	public int getCompensationNumber() {
		return compensationNumber;
	}
	public void setCompensationNumber(int compensationNumber) {
		this.compensationNumber = compensationNumber;
	}
	public int getSuccessNumber() {
		return successNumber;
	}
	public void setSuccessNumber(int successNumber) {
		this.successNumber = successNumber;
	}
	public int getCheckpointNumber() {
		return checkpointNumber;
	}
	public void setCheckpointNumber(int checkpointNumber) {
		this.checkpointNumber = checkpointNumber;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public double getExecutionTime() {
		return executionTime;
	}
	public void setExecutionTime(double executionTime) {
		this.executionTime = executionTime;
	}
	public int getServiceCount() {
		return serviceCount;
	}
	public void setServiceCount(int serviceCount) {
		this.serviceCount = serviceCount;
	}
	public CompositeTransactionalProperty getTransactionalProperty() {
		return transactionalProperty;
	}
	public void setTransactionalProperty(CompositeTransactionalProperty transactionalProperty) {
		this.transactionalProperty = transactionalProperty;
	}

	public void validate() {
		try {
			if(success && (compensation || checkpointing))
				throw new Exception("success & (compensation || checkpointing)");
			if(success && (compensationNumber > 0 || checkpointNumber > 0))
				throw new Exception("success & (compensationNumber > 0 || checkpointNumber > 0)");
			if(compensation && checkpointing)
				throw new Exception("compensation && checkpointing");
			if(failureServiceNumber > failureNumber)
				throw new Exception("failureServiceNumber > failureNumber");
				
			logger.info("Execution Result validated");
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	public List<String> getFailedServices() {
		return failedServices;
	}
	public void setFailedServices(List<String> failedServices) {
		this.failedServices = failedServices;
	}
	public List<String> getSuccessfulServices() {
		return successfulServices;
	}
	public void setSuccessfulServices(List<String> successfulServices) {
		this.successfulServices = successfulServices;
	}
	public java.util.Map<String, List<Double>> getFailureTimesByService() {
		return failureTimesByService;
	}
	public void setFailureTimesByService(java.util.Map<String, List<Double>> failureTimesByService2) {
		this.failureTimesByService = failureTimesByService2;
	}
	public List<String> getProducedOutputs() {
		return producedOutputs;
	}
	public void setProducedOutputs(List<String> producedOutputs) {
		this.producedOutputs = producedOutputs;
	}
	public Map<String,Integer> getRetryNumberByService() {
		return retryNumberByService;
	}
	public void setRetryNumberByService(Map<String,Integer> retryNumberByService) {
		this.retryNumberByService = retryNumberByService;
	}
	public Map<String,Integer> getSubstitutionNumberByService() {
		return substitutionNumberByService;
	}
	public void setSubstitutionNumberByService(
			Map<String,Integer> substitutionNumberByService) {
		this.substitutionNumberByService = substitutionNumberByService;
	}
	public void setAccumulatedTime(double accumulatedTime) {
		this.accumulatedTime = accumulatedTime;
		
	}
	public double getAccumulatedTime() {
		return this.accumulatedTime;
	}
	public double getCompensateMessageReceivedAt() {
		return compensateMessageReceivedAt;
	}
	public void setCompensateMessageReceivedAt(double compensateMessageReceivedAt) {
		this.compensateMessageReceivedAt = compensateMessageReceivedAt;
	}
	public List<String> getCompensatedServices() {
		return compensatedServices;
	}
	public void setCompensatedServices(List<String> compensatedServices) {
		this.compensatedServices = compensatedServices;
	}
	public List<Analysis> getSelfHealingAnalysis() {
		return selfHealingAnalysis;
	}
	public void setSelfHealingAnalysis(List<Analysis> selfHealingAnalysis) {
		this.selfHealingAnalysis = selfHealingAnalysis;
	}
	
}
