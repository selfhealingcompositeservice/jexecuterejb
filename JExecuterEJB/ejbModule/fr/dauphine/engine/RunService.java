package fr.dauphine.engine;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import fr.dauphine.service.Data;
import fr.dauphine.service.Service;


/**
 * Handles the execution of a Service
 *
 * @author Rafael Angarita
 * @version 1.0
 */
public class RunService {
	
	//normal execution
	Service service;
	//replication
	List<Service> services;
	double executionTime;
	boolean success;
	Timestamp timeStamp;
	double price;
	double reputation;
	int failuresNumber;
	
	
	private static final Logger logger = Logger.getLogger(RunService.class.getCanonicalName());
	
	public RunService(Service service) {
		this.service = service;
	}
	
	public RunService(List<Service> services) {
		this.services = services;
	}
	
	public void run() {
		
		if(this.service != null)
			performNormalExecution();
		else
			performReplicatedExecution();
		

		
	}
	
	private void performReplicatedExecution() {
		try {
		double minExecutionTime = Double.POSITIVE_INFINITY; //the time of the replica that finished first
		double maxExecutionTime = Double.NEGATIVE_INFINITY; //maximum waiting time
		
		String name = ""; //the name of the successful replica
		String nameMax = ""; //the name of the successful replica
		for(Service s:services) {
			if(simulateServiceFailure(s) && s.getEstimatedExecutionTime() < minExecutionTime) {
				minExecutionTime = s.getEstimatedExecutionTime();
				name = s.getName();
			} if(s.getEstimatedExecutionTime() > maxExecutionTime) {
				maxExecutionTime = s.getEstimatedExecutionTime();
				nameMax = s.getName();
			}
		}
		
		if(minExecutionTime < Double.POSITIVE_INFINITY) {
			success = true;
			logger.log(Level.INFO, "success_replica={" + name + "," + success + "}"); 
			try {
				executionTime = minExecutionTime;
				Thread.sleep((long) executionTime);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		} else {
			success = false;
			logger.log(Level.INFO, "success_replica={" + nameMax + "," + success + "}"); 
				executionTime = maxExecutionTime;
				Thread.sleep((long) executionTime);
			
		}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	private void performNormalExecution() {
		try {
			success = simulateServiceFailure(this.service);
			
			logger.log(Level.INFO, "success={" + service.getName() + "," + success + "}"); 
			
			executionTime = simulateExecutionTime(success);
			java.util.Date currentDate = new java.util.Date();
			timeStamp = new Timestamp(currentDate.getTime());
			
			if(executionTime >= 0)
				Thread.sleep((long) executionTime);
			

		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public double getExecutionTime() {
		return executionTime;
	}
	
	public boolean getSuccess() {
		return success;
	}
	
	public Timestamp getTimeStamp() {
		return timeStamp;
	}
	
	public double getPrice() {
		return price;
	}

	public double getReputation() {
		return reputation;
	}

	public int getFailuresNumber() {
		return failuresNumber;
	}

	//simulate execution time
	private double simulateExecutionTime(boolean success) {
		/*if(!success && service.getEstimatedExecutionTime() > 0)
			return randInt(0, service.getEstimatedExecutionTime() +  service.getEstimatedExecutionTime());
		else*/
			return service.getEstimatedExecutionTime();
			//return randInt(service.getEstimatedExecutionTime(), service.getEstimatedExecutionTime() + service.getEstimatedExecutionTime()*0.0);
	}

	//simulate probabilty of a failure
	//returns true if the service fails
	private boolean simulateServiceFailure(Service s) {
		if(Math.random() < 1-s.getAvailability())
		   return false;
		return true;
	}
	
	private static int randInt(double min, double max) {
	    Random rand = new Random();
	    int randomNum = (int) (rand.nextInt((int) ((max - min) + 1)) + min);

	    return randomNum;
	}
	
	//dummy output generation
	public Map<Data, String> dummyGenerateOutputs() {
		Map<Data, String> outputs = new HashMap<Data, String>();
		for(Data o:service.getOutputs())
			outputs.put(o, o.getName());
		return outputs;
	}

}