package fr.dauphine.test;

import java.io.File;

import edu.uci.ics.jung.graph.Graph;
import fr.dauphine.data.DataBean;
import fr.dauphine.generator.RandomGeneratorBean;
import fr.dauphine.graphanalyser.GraphAnalyser;
import fr.dauphine.service.Service;

public class CWSAnalysis {
	
	public static void generateCompositeServices(int number, int minSize, int maxSize) {
		System.out.println("Generating " + number + " CWSs from size " + minSize + " to " + maxSize);
		RandomGeneratorBean genBean = new RandomGeneratorBean();
		DataBean dataBean = new DataBean();
		
		String graphDir = "graphs";
		fr.dauphine.utils.file.File.checkDirectory(graphDir);
		
		for(int size=minSize;  size<=maxSize;  size++) {
			System.out.println("Size " + size);
			for(int j=1; j<=number; j++) {
				if(number > 90) {
					String dir = graphDir + "/" + size;
					fr.dauphine.utils.file.File.checkDirectory(dir);
					Graph<Service, Number> g = genBean.generateRandomGraph(size);
					dataBean.saveGraph(g, dir +  "/" + size + "_" + j);
				}
			}
		}
		
		System.out.println("End generation");
		
	}
	
	public static void analyseCompositeServices(int number, int minSize, int maxSize) {
		String graphDir = "graphsAnalysis";
		DataBean dataBean = new DataBean();
		for(int size=minSize;  size<=maxSize;  size++) {
			for(int j=1; j<=number; j++) {
				String dir = graphDir;
				Graph<Service, Number> graph = dataBean.readGraph("graphs" +  "/" + size  + "/"+ size + "_" + j);
				System.out.println("size " + graph.getVertexCount());
				GraphAnalyser analyser = new GraphAnalyser(graph);
				String data = "size=" + graph.getVertexCount() + ", ";
				data += "tp=" + analyser.getTransactionalProperty() + ", ";
				data += "time=" + analyser.getEstimatedExecutionTime() + ", ";
				data += "price=" + analyser.getEstimatedPrice() + ", ";
				data += "availability=" + analyser.getEstimatedAvailability() + "\n";
				dataBean.appendStringToFile(data, graphDir +  "/" + size + ".analysis");
				
			}
		}
	}
	
	
	public static void main(String...args) {
		//generated 1000 CWS for each size from 2 to 1000 services
		//generateCompositeServices(100, 2, 1000);
		analyseCompositeServices(100, 986, 1000);
	}

}
