package fr.dauphine.data;

import javax.ejb.Remote;

import edu.uci.ics.jung.graph.Graph;
import fr.dauphine.engine.ExecutionResult;
import fr.dauphine.service.Service;

@Remote
public interface IDataBean {

	void save(ExecutionResult executionResult, Graph<Service, Number> graph);
	Graph<Service, Number> readGraph(String path);
}
