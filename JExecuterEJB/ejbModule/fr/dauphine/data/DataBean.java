package fr.dauphine.data;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import javax.ejb.Stateless;

import org.apache.commons.io.FileUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import edu.uci.ics.jung.graph.Graph;
import fr.dauphine.engine.ExecutionResult;
import fr.dauphine.engine.kb.Analysis;
import fr.dauphine.graphutils.JSONUtils;
import fr.dauphine.service.Service;

@Stateless
public class DataBean implements IDataBean {

	public DataBean() {
	}

	@Override
	public void save(ExecutionResult executionResult, Graph<Service,Number> graph) {
		
		String name = "BA_" + graph.getVertexCount();
		File file = new File(name);
		
		
		String data = "";
		
		//number of vertices
		data += "size=" + new Integer(executionResult.getServiceCount()).toString();
		
		//transactional property
		data += ", tp=" + executionResult.getTransactionalProperty();
		
		//framework execution time
		data += ", framework_et=" + new Double(executionResult.getExecutionEngineTime());
		
		//accumulated execution time
		data += ", accumulated_et=" + new Double(executionResult.getAccumulatedTime());
		
		//execution time when compensation message was received
		data += ", compensate_message_time=" + new Double(executionResult.getCompensateMessageReceivedAt());
		
		//estimated QoS
		data += ", eet=" + new Double(executionResult.getEstimatedExecutionTime()).toString();
		data += ", eep=" + new Double(executionResult.getEstimatedPrice()).toString();
		data += ", eea=" + new Double(executionResult.getEstimatedAvailability()).toString();
		
		//real QoS
		data += ", et=" + new Double(executionResult.getExecutionTime()).toString();
		data += ", ep=" + executionResult.getPrice();
		
		//used techniques
		data += ", success=" + executionResult.isSuccess();
		data += ", retry=" + executionResult.isRetry();
		data += ", substitution=" + executionResult.isSubstitution();
		data += ", compensation=" + executionResult.isCompensation();
		data += ", checkpointing=" + executionResult.isCheckpointing();
		
		
		//number of failures, substitutions, etc
		data += ", total_failures=" + executionResult.getFailureNumber();
		data += ", failed_services=" + executionResult.getFailureServiceNumber();//number of services that failed
		
		data += ", compensated_services=" + executionResult.getCompensationNumber();
		data += ", successful_services=" + executionResult.getSuccessNumber();
		
		data += ", total_substitutions=" + executionResult.getSubstitutionNumber();
		data += ", total_retry=" + executionResult.getRetryNumber();
		data += ", retry_services=" + executionResult.getRetryNumberServices();
		data += ", substitution_services=" + executionResult.getSubstitutionNumberServices();
		data += ", checkpointed_services=" + executionResult.getCheckpointNumber();
		
		//lists
		data += ", successful_services_names={";
		
		for(int i=0; i<executionResult.getSuccessfulServices().size(); i++) {
			String sName = executionResult.getSuccessfulServices().get(i);
			data += sName;
			if(i<executionResult.getSuccessfulServices().size()-1)
				data += ";";
		}
		
		data += "}";
		
		
		data += ", failed_services_names={";
		
		for(int i=0; i<executionResult.getFailedServices().size(); i++) {
			String sName = executionResult.getFailedServices().get(i);
			data += sName + ":";
			for(int d=0; d<executionResult.getFailureTimesByService().get(sName).size(); d++) {
				data += executionResult.getFailureTimesByService().get(sName).get(d);
				if(d<executionResult.getFailureTimesByService().get(sName).size()-1)
					data += "-";
			}
			if(i<executionResult.getFailedServices().size()-1)
				data += ";";
		}
		
		data += "}";
		
		data += ", compensated_services_names={";
		
		for(int i=0; i<executionResult.getCompensatedServices().size(); i++) {
			String sName = executionResult.getCompensatedServices().get(i);
			data += sName;
			if(i<executionResult.getCompensatedServices().size()-1)
				data += ";";
		}
		
		data += "}";
		
		data += ", produced_outputs={";
		
		for(int i=0; i<executionResult.getProducedOutputs().size(); i++) {
			String sName = executionResult.getProducedOutputs().get(i);
			data += sName ;
			if(i<executionResult.getProducedOutputs().size()-1)
				data += ";";
		}
		
		data += "}";
		
		data += ", retry_number_service={";
		
		int i= 0;
		for(String s:executionResult.getRetryNumberByService().keySet()) {
			data += s + ":" + executionResult.getRetryNumberByService().get(s);
			if(i<executionResult.getRetryNumberByService().size()-1)
				data += ";";
		}
		data += "}";
		
		data += ", substitution_number_service=" + executionResult.getSubstitutionNumberByService();
		
		
		data += ", self_healing={";
		
		for(int j=0; j<executionResult.getSelfHealingAnalysis().size(); j++) {
			Analysis a = executionResult.getSelfHealingAnalysis().get(j);
			data += a.getServiceName() + ":";
			data += "analysis_type&" + a.getAnalysisType();
			data += "$";
			data += "analysis_time&" + a.getAnalysisTime();
			data += "$";
			data += "state&" + a.getSelfhealingState();
			data += "$";
			data += "previous_state&" + a.getPreviousSelfhealingState();
			data += "$";
			data += "actual_consumed_time&" + a.getActualConsumedTime();
			data += "$";
			data += "local_expected_time&" + a.getLocalExpectedTime();
			data += "$";
			data += "local_remaining_time&" + a.getLocalRemainingTime();
			data += "$";
			data += "updated_free_time&" + a.getUpdatedFreeTime();
			data += "$";
			data += "global_eet&" + a.getGlobalExpectedTime();
			data += "$";
			data += "local_cp&" + a.getLocalCriticalPath();
			data += "$";
			data += "updated_local_cp&" + a.getUpdatedGlobalExpectedTime();
			data += "$";
			data += "exceeding_percentage&" + a.getExceedingPercentage();
			data += "$";
			data += "transactional_property&" + a.getTransactionalProperty();
			data += "$";
			data += "time_threshold&" + a.getTimeThreshold();
			data += "$";
			data += "recovery_plan&" + a.getRecoveryPlan();
			data += "$";
			data += "default_recovery_plan&" + a.getDefaultRecoveryPlan();
			
			if(j<executionResult.getSelfHealingAnalysis().size()-1)
				data += ";";
		}
		
		data += "}";
		
		data += "\n";
		try {
			FileUtils.writeStringToFile(file, data, true);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		//save graph
		saveGraph(graph, name+"_graph");
        
            
	}
	
	public void saveGraph(Graph<Service, Number> graph, String path) {
		try {
        	FileWriter fileGraph = new FileWriter(path);
        	fileGraph.write(JSONUtils.graphToJson(graph).toString());
        	fileGraph.close();
        } catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public Graph<Service, Number> readGraph(String path) {
		JSONParser parser = new JSONParser();
		try {
			Object obj = parser.parse(new FileReader(path));
			return JSONUtils.jsonToGraph((JSONObject)obj);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public void appendStringToFile(String data, String path) {
		try {
			FileUtils.writeStringToFile(new File(path), data, true);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	
}
