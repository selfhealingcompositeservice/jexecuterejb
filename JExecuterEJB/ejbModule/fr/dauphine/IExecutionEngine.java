package fr.dauphine;

import javax.ejb.Remote;

import edu.uci.ics.jung.graph.Graph;
import fr.dauphine.engine.ExecutionPreferences;
import fr.dauphine.engine.ExecutionResult;
import fr.dauphine.service.Service;

@Remote
public interface IExecutionEngine {

	ExecutionResult run(Graph<Service, Number> graph, ExecutionPreferences preferences);
}
