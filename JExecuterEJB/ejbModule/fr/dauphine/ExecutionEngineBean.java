package fr.dauphine;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.enterprise.concurrent.ManagedThreadFactory;
import javax.xml.ws.WebServiceRef;

import org.apache.commons.collections15.Factory;

import com.emergencyservices.services.CallAmbulanceService;
import com.emergencyservices.services.CallAmbulanceServiceService;

import jersey.repackaged.com.google.common.base.Stopwatch;
import edu.uci.ics.jung.graph.DirectedSparseMultigraph;
import edu.uci.ics.jung.graph.Graph;
import fr.dauphine.algorithms.LongestPathTopological;
import fr.dauphine.algorithms.TopologicalSort;
import fr.dauphine.engine.EngineThread;
import fr.dauphine.engine.EngineThreadContext;
import fr.dauphine.engine.ExecutionPreferences;
import fr.dauphine.engine.ExecutionResult;
import fr.dauphine.engine.kb.Analysis;
import fr.dauphine.graphanalyser.GraphAnalyser;
import fr.dauphine.graphanalyser.TimeContext;
import fr.dauphine.graphutils.GraphUtils;
import fr.dauphine.message.ControlMessage;
import fr.dauphine.message.ControlMessageType;
import fr.dauphine.message.DataMessage;
import fr.dauphine.message.Message;
import fr.dauphine.message.MessageFactory;
import fr.dauphine.service.Constants;
import fr.dauphine.service.Data;
import fr.dauphine.service.Service;

/**
 * Session Bean implementation class ExecutionEngineBean
 */
@Stateless
@Remote(IExecutionEngine.class)
public class ExecutionEngineBean implements IExecutionEngine {
	
	private static final Logger logger = Logger.getLogger("ExecutionEngine");
	
	private Stopwatch stopwatch;
	
	@Resource
    private ManagedThreadFactory threadFactory;
	
	private Map<String, LinkedBlockingDeque<Message>> queues;
	
	private Graph<Service, Number> graph;
	
	private Map<String, String> receivedOutputs;
	
	private MessageFactory messageFactory;
	
	private List<EngineThread> threads;
	
	private double accumulatedTime = 0.0;
	
	private double compensateMessageReceivedAt;
	
	/**
	 * Config
	 */
	
	private ExecutionPreferences preferences;

	
 
    /**
     * Default constructor. 
     */
    public ExecutionEngineBean() {
    	
    }
   
   
    
	@Override
	public ExecutionResult run(Graph<Service, Number> graph, ExecutionPreferences preferences) {
		accumulatedTime = 0.0;
		this.preferences = preferences;
		
		this.graph = graph;
		System.out.println("Run: " + graph);
		
		threads = new ArrayList<>();
		
		GraphUtils.printServices(graph);
		GraphUtils.addControlNodes(graph);
		
		//save results
		ExecutionResult executionResult = new ExecutionResult();
		
		//vertices number
		executionResult.setServiceCount(graph.getVertexCount()-2);
		
		GraphAnalyser analyser  = new GraphAnalyser(graph);
		
		//transactional property
		executionResult.setTransactionalProperty(analyser.getTransactionalProperty());
		
		//estimated QoS
		double estimatedExecutionTime = analyser.getEstimatedExecutionTime();
		executionResult.setEstimatedExecutionTime(estimatedExecutionTime);
		executionResult.setEstimatedPrice(analyser.getEstimatedPrice());
		executionResult.setEstimatedAvailability(analyser.getEstimatedAvailability());
		
		//Engine Thread Context
		Map<String, Set<String>> allPredecessors = analyser.getAllPredecessors();
		Map<String, Set<Data>> outputDependencies = analyser.getOutputDependency();
		analyser  = new GraphAnalyser(graph);
		Map<String, TimeContext> timeContexts = analyser.getLocalTimeContexts();
		
		Map<String, Double> topResult = new HashMap<>();
		for(Service s:graph.getVertices()) {
			topResult = LongestPathTopological.getLongestPath(graph, s.getName(), TopologicalSort.sort(graph));
		}
		System.out.println(topResult);
		
		
		queues = new HashMap<String, LinkedBlockingDeque<Message>>();
    	queues.put(Constants.FINAL_NODE, new LinkedBlockingDeque<Message>());
    	
    	messageFactory = new MessageFactory(queues);
    	
    	receivedOutputs = buildInputList();
    	/**
    	 * Initialization
    	 */
		for(Service s:graph.getVertices())
			if(!s.isControl()) {
	    		queues.put(s.getName(), new LinkedBlockingDeque<Message>());
	    		EngineThreadContext threadContext = new EngineThreadContext(
	    				allPredecessors.get(s.getName()),
	    				outputDependencies.get(s.getName()),
	    				graph.getVertexCount()-2,
	    				receivedOutputs.keySet().size(),
	    				timeContexts.get(s.getName())
	    				);
		        EngineThread reportTask = new EngineThread(s,
		        		GraphUtils.getPredecessors(graph, s),
		        		GraphUtils.getSuccessors(graph, s),
		        		queues.get(s.getName()),
		        		messageFactory, preferences, threadContext);
		        threads.add(reportTask);
		        Thread thread = threadFactory.newThread(reportTask);
		        thread.start();
			}
		
		
		
		Service wseei = GraphUtils.getVertexByName(graph, Constants.INITIAL_NODE);
		System.out.println("CWS inputs " + wseei.getOutputs() + " to " + graph.getSuccessors(wseei));
		
		stopwatch = new Stopwatch();
		stopwatch.start();
		
		/**
    	 * Execution
    	 */
		//Send composite Service inputs
		List<Message> inputs = new ArrayList<>();
		for(Data d:wseei.getOutputs())
			inputs.add(new DataMessage(d, d.getName()));
		

		messageFactory.send(inputs, graph.getSuccessors(wseei));
		
		
		
		
		/**
    	 * Execution Engine Final Phase.
    	 */
		while(!isFireable()) {
			waitMessage();
		}
		
		double elapsed = stopwatch.elapsed(java.util.concurrent.TimeUnit.MILLISECONDS);
		
		logger.log(Level.INFO, "CWS outputs={" + receivedOutputs + "}");
		
		
		//send terminate message to all Engine Threads
		for(Service s:graph.getVertices()) {
			if(!s.isControl()) {
				logger.info("Execution Engine send terminate token to  " + s.getName());
				Message msg = MessageFactory.getTerminateToken();
				msg.setSender(Constants.FINAL_NODE);
				messageFactory.send(msg, s);
			}
		}
		
		//save execution data
		
		executionResult.setExecutionEngineTime(elapsed);
		
		
		boolean success = true, substitution = false, compensation= false, checkpointing = false;
		int successNumber = 0, retryNumber = 0, failureServiceNumber = 0, failureNumber = 0, retryNumberServices = 0, substitutionNumberServices = 0
			, substitutionNumber = 0	, compensatedNumber = 0;
		List<String> failedServices = new ArrayList<>();
		List<String> successfulServices = new ArrayList<>();
		List<String> compensatedServices = new ArrayList<>();
		Map<String,List<Double>> failureTimesByService = new HashMap<>();
		Map<String,Integer> retryNumberByService = new HashMap<>();
		Map<String,Integer> substitutionNumberByService = new HashMap<>();
		
		List<Analysis> selfHealing = new ArrayList<>();
		
		for(EngineThread t:threads) {
			Service s = GraphUtils.getVertexByName(graph, t.getName());
			s.setEstimatedExecutionTime(t.getTotalExecutionTime());
			s.setPrice(t.getTotalPrice());
			retryNumber += t.getRetryNumber();
			substitutionNumber += t.getSubstitutionNumber();
			
			selfHealing.addAll(t.getAnalysis());
			
			if(t.isFailed()) {
				failureServiceNumber++;
				failedServices.add(t.getName());
				failureTimesByService.put(t.getName(), t.getFailureTimes());
			}
			if(t.getRetryNumber() > 0) {
				failureNumber += t.getRetryNumber()-1;
				retryNumberServices++;
				retryNumberByService.put(t.getName(), new Integer(t.getRetryNumber()));
			}
			if(t.getSubstitutionNumber() > 0) {
				failureNumber += t.getSubstitutionNumber()-1;
				substitutionNumberServices++;
				substitutionNumberByService.put(t.getName(), new Integer(t.getRetryNumber()));
			}
			
			
			
			switch (t.getState()) {
			case COMPENSATED:
				compensation = true;
				success = false;
				compensatedNumber++;
				compensatedServices.add(t.getName());
				break;
			case CHECKPOINTED:
				checkpointing = true;
				success = false;
				failureServiceNumber++;
				
				break;
				
			case SKIPPED:
				checkpointing = true;
				success = false;
				break;
				
			case EXECUTED:
				successNumber++;
				successfulServices.add(t.getName());
				break;
				
			case ABANDONED:
				compensation = true;
				success = false;
				break;
			case FAILED:
				success = false;
				break;
			default:
				break;
			}
		}
		
		executionResult.setAccumulatedTime(accumulatedTime);
		executionResult.setCompensateMessageReceivedAt(compensateMessageReceivedAt);
		
		failureNumber += failureServiceNumber;
		
		analyser = new GraphAnalyser(graph);
		//real QoS
		executionResult.setExecutionTime(analyser.getEstimatedExecutionTime());
		executionResult.setPrice(analyser.getEstimatedPrice());
		
		//used techniques
		executionResult.setSuccess(success);
		executionResult.setRetry(retryNumber>0 ? true:false);
		executionResult.setSubstitution(substitution);
		executionResult.setCompensation(compensation);
		executionResult.setCheckpointing(checkpointing);
		
		//counts
		executionResult.setSuccessNumber(successNumber);
		executionResult.setRetryNumber(retryNumber);
		executionResult.setRetryNumberServices(retryNumberServices);
		executionResult.setFailureNumber(failureNumber);
		executionResult.setFailureServiceNumber(failureServiceNumber);
		executionResult.setCompensationNumber(compensatedNumber);
		executionResult.setSubstitutionNumber(substitutionNumber);
		executionResult.setSubstitutionNumberServices(substitutionNumberServices);
		
		executionResult.setFailureTimesByService(failureTimesByService);
		
		executionResult.validate();
		
		//lists
		executionResult.setFailedServices(failedServices);
		executionResult.setSuccessfulServices(successfulServices);
		executionResult.setProducedOutputs(new ArrayList<String>(receivedOutputs.values()));
		executionResult.setCompensatedServices(compensatedServices);
		
		//retry number by service
		executionResult.setRetryNumberByService(retryNumberByService);
		//substitution number by service
		executionResult.setSubstitutionNumberByService(substitutionNumberByService);
		
		//self-healing
		executionResult.setSelfHealingAnalysis(selfHealing);
		
		return executionResult;
	}
	
	private Map<String, String> buildInputList() {
		Map<String, String> inputList = new HashMap<>();
		
		for(Data data:GraphUtils.getVertexByName(graph, Constants.FINAL_NODE).getInputs())
			inputList.put(data.getName(), null);
		
		return inputList;
	}
	
	private void waitMessage() {
		try {
			boolean initiatedCompensation = false;
			logger.log(Level.INFO, "Execution Engine take: " + queues.get(Constants.FINAL_NODE));
			Message msg = queues.get(Constants.FINAL_NODE).take();
			if(msg instanceof ControlMessage) {
				ControlMessage cmsg = (ControlMessage) msg;
				
				if(cmsg.getMessage().equals(ControlMessageType.COMPENSATE) && !initiatedCompensation) {
					compensateMessageReceivedAt = (double) stopwatch.elapsed(java.util.concurrent.TimeUnit.MILLISECONDS);
					initiatedCompensation = true;
					compensate();
				} else if(cmsg.getMessage().equals(ControlMessageType.COMPENSATE_TOKEN)) {
					updateAccumulatedTime(cmsg.getAccumulatedTime());
					receivedOutputs.put(cmsg.getSender(), ControlMessageType.COMPENSATE_TOKEN.toString());
				}
			} else if(msg instanceof DataMessage) {
				DataMessage dmsg = (DataMessage) msg;
				updateAccumulatedTime(dmsg.getAccumulatedTime());
				receivedOutputs.put(dmsg.getData().getName(), dmsg.getValue());
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	private void updateAccumulatedTime(double accumulatedTime) {
		System.out.println("EE update " + this.accumulatedTime + "- " + accumulatedTime);
		this.accumulatedTime = accumulatedTime > this.accumulatedTime ? accumulatedTime:this.accumulatedTime;
	}

	private void compensate() {
		// send compensate message to all Engine Threads
		for(Service s:graph.getVertices())
			if(!s.isControl())
				messageFactory.send(MessageFactory.getCompensateMessage(), s);
		
		
		//set expected outputs to modify the waiting condition
		
		receivedOutputs = new HashMap<>();
		
		for(Service s:graph.getSuccessors(GraphUtils.getVertexByName(graph, Constants.INITIAL_NODE)))
			receivedOutputs.put(s.getName(), null);
		
		
		//starts compensation by sending compensate tokens
		logger.info("Execution Engine predecessors " + graph.getPredecessors(GraphUtils.getVertexByName(graph, Constants.FINAL_NODE)));
		for(Service s:graph.getPredecessors(GraphUtils.getVertexByName(graph, Constants.FINAL_NODE))) {
			logger.info("Execution Engine send compensation token to  " + s.getName());
			Message msg = MessageFactory.getCompensateTokenMessage();
			msg.setSender(Constants.FINAL_NODE);
			messageFactory.send(msg, s);
		}
		
	}

	private boolean isFireable() {
		return !receivedOutputs.containsValue(null);
	}
 
}
